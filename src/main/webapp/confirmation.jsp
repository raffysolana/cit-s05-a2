<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration Confirmation</title>
</head>
<body>
	<%
		 String discover = session.getAttribute("discover").toString();
		String message;
		
		if(discover.equals("Friends")){
			discover="Friends";
		}else if(discover.equals("Social Media")){
			discover="Social Media";
		}else{
			discover="Others";
		} 
		
		String dateOfBirth = session.getAttribute("dateOfBirth").toString();

	%>

	<h1>Registration Confirmation</h1>
	<p>First Name: <%=session.getAttribute("firstname") %></p>
	<p>Last Name: <%=session.getAttribute("lastname") %></p>
	<p>Phone Number: <%=session.getAttribute("phone") %></p>
	<p>Email: <%=session.getAttribute("email") %></p>
	<p>App Discovery: <%=discover%></p>
	<p>Date of Birth: <%=dateOfBirth %></p>
	<p>User Type: <%=session.getAttribute("typeOfWork") %></p>
	<p>Description: <%=session.getAttribute("description") %></p>
	
	<form action ="welcome.jsp" method="post">
		<input type="submit">
	</form>
	
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>
	
</body>
</html>
